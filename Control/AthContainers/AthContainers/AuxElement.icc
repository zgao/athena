// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file AthContainers/AuxElement.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2013
 * @brief Base class for elements of a container that can have aux data.
 */


#include "AthContainers/tools/likely.h"
#include "CxxUtils/checker_macros.h"
#include <cassert>


namespace SG {


/**
 * @brief Default constructor.
 */
inline
ConstAuxElement::ConstAuxElement() 
  : m_container (nullptr)
{
}


/**
 * @brief Constructor with explicit container / index.
 * @param container Container of which this element will be a part.
 * @param index Index of this element within the container.
 *
 * This does not make any changes to aux data.
 */
inline
ConstAuxElement::ConstAuxElement (const SG::AuxVectorData* container,
                                  size_t index)
  : IAuxElement (index),
    m_container (container)
{
}


/**
 * @brief Copy Constructor.
 * @param other Object being copied.
 *
 * We do not copy the container/index --- the new object is not yet
 * in a container!
 *
 * In the case of constructing an object with a private store,
 * @c makePrivateStore will take care of copying the aux data.
 */
inline
ConstAuxElement::ConstAuxElement (const ConstAuxElement& /*other*/)
  : IAuxElement(),
    m_container (nullptr)
{
}


/**
 * @brief Destructor.
 *
 * Any private store is deleted.
 */
inline
ConstAuxElement::~ConstAuxElement()
{
  if (ATHCONTAINERS_UNLIKELY (!noPrivateData()))
    releasePrivateStoreForDtor();
}


/**
 * @brief Return the container holding this element.
 */
inline
const SG::AuxVectorData* ConstAuxElement::container() const
{
  return m_container;
}


/**
 * @brief Set the index/container for this element.
 * @param index The index of this object within the container.
 * @param container The container holding this object.
 *                  May be null if this object is being removed
 *                  from a container.
 *
 * Usually this simply sets the index and container members
 * of this object.  However, in the case where this object has
 * an associated private store, then we need to deal with
 * releasing the store if the object is being added to a container,
 * or making a new store if the object is being removed from a container.
 */
inline
void ConstAuxElement::setIndex (size_t index,
                                const SG::AuxVectorData* container)
{
  if (ATHCONTAINERS_UNLIKELY (!noPrivateData())) {
    // out-of-line piece, dealing with private store.
    setIndexPrivate (index, container);
    return;
  }

  IAuxElement::setIndex (index);
  m_container = container;
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor
 * or @c ConstAccessor classes.
 */
template <class T, class ALLOC>
typename ConstAccessor<T, ALLOC>::const_reference_type
ConstAuxElement::auxdata (const std::string& name) const
{
  return ConstAccessor<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor
 * or @c ConstAccessor classes.
 */
template <class T, class ALLOC>
typename ConstAccessor<T, ALLOC>::const_reference_type
ConstAuxElement::auxdata (const std::string& name,
                          const std::string& clsname) const
{
  return ConstAccessor<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c ConstAccessor class.
 */
template <class T, class ALLOC>
typename ConstAccessor<T, ALLOC>::const_reference_type
ConstAuxElement::auxdataConst (const std::string& name) const
{
  return ConstAccessor<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c ConstAccessor class.
 */
template <class T, class ALLOC>
typename ConstAccessor<T, ALLOC>::const_reference_type
ConstAuxElement::auxdataConst (const std::string& name,
                               const std::string& clsname) const
{
  return ConstAccessor<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Check if an aux variable is available for reading
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
bool ConstAuxElement::isAvailable (const std::string& name,
                                   const std::string& clsname /*= ""*/) const
{
  return ConstAccessor<T, ALLOC>(name, clsname).isAvailable(*this);
}


/**
 * @brief Check if an aux variable is available for writing as a decoration.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
bool ConstAuxElement::isAvailableWritableAsDecoration (const std::string& name,
                                                       const std::string& clsname /*= ""*/) const
{
  return Decorator<T, ALLOC>(name, clsname).isAvailableWritable(*this);
}


/**
 * @brief Fetch an aux decoration, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
typename Decorator<T, ALLOC>::reference_type
ConstAuxElement::auxdecor (const std::string& name) const
{
  return Decorator<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux decoration, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
typename Decorator<T, ALLOC>::reference_type
ConstAuxElement::auxdecor (const std::string& name,
                           const std::string& clsname) const
{
  return Decorator<T, ALLOC>(name, clsname)(*this);
}


//******************************************************************************


/**
 * @brief Default constructor.
 */
inline
AuxElement::AuxElement()
#ifdef ATHCONTAINERS_R21_COMPAT
  : m_container(0)
#endif
{
}


/**
 * @brief Constructor with explicit container / index.
 * @param container Container of which this element will be a part.
 * @param index Index of this element within the container.
 *
 * This does not make any changes to aux data.
 */
inline
AuxElement::AuxElement (SG::AuxVectorData* container,
                        size_t index)
#ifdef ATHCONTAINERS_R21_COMPAT
  : IAuxElement(index),
    m_container (container)
#else
  : ConstAuxElement(container, index)
#endif
{
}


/**
 * @brief Copy Constructor.
 * @param other Object being copied.
 *
 * We do not copy the container/index --- the new object is not yet
 * in a container!
 *
 * In the case of constructing an object with a private store,
 * @c makePrivateStore will take care of copying the aux data.
 */
inline
AuxElement::AuxElement ([[maybe_unused]] const AuxElement& other)
#ifdef ATHCONTAINERS_R21_COMPAT
  : IAuxElement(),
    m_container(0)
#else
  : ConstAuxElement (other)
#endif
{
}


/**
 * @brief Assignment.
 * @param other The object from which we're assigning.
 *
 * We don't copy container/index, as assignment doesn't change where
 * this object is.  However, if we have aux data, then we copy aux data
 * if we're copying from an object that also has it; otherwise,
 * if we're copying from an object with no aux data, then we clear ours.
 */
inline
AuxElement& AuxElement::operator= (const AuxElement& other)
{
  if (this != &other) {
    if (this->container()) {
      if (!other.container()) {
        // Copying from an object with no aux data.
        // Clear our aux data.
        this->clearAux();
      }
      else {
        // Copying from an object with aux data.
        // Copy the aux data too.
        this->copyAux (other);
      }
    }
  }
  return *this;
}


/**
 * @brief Destructor.
 *
 * Any private store is deleted.
 */
inline
AuxElement::~AuxElement()
{
#ifdef ATHCONTAINERS_R21_COMPAT
  if (ATHCONTAINERS_UNLIKELY (!noPrivateData()))
    releasePrivateStoreForDtor();
#endif
}


/**
 * @brief Return the container holding this element.
 */
inline
const SG::AuxVectorData* AuxElement::container() const
{
#ifdef ATHCONTAINERS_R21_COMPAT
  return m_container;
#else
  return ConstAuxElement::container();
#endif
}


/**
 * @brief Return the container holding this element.
 */
inline
SG::AuxVectorData* AuxElement::container()
{
#ifdef ATHCONTAINERS_R21_COMPAT
  return m_container;
#else
  SG::AuxVectorData* cont ATLAS_THREAD_SAFE = const_cast<SG::AuxVectorData*> (ConstAuxElement::container());
  return cont;
#endif
}


//******************************************************************************


/**
 * @brief Fetch an aux data variable, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::reference_type
AuxElement::auxdata (const std::string& name)
{
  return Accessor<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux data variable, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::reference_type
AuxElement::auxdata (const std::string& name,
                     const std::string& clsname)
{
  return Accessor<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor
 * or @c ConstAccessor classes.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::const_reference_type
AuxElement::auxdata (const std::string& name) const
{
  return Accessor<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor
 * or @c ConstAccessor classes.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::const_reference_type
AuxElement::auxdata (const std::string& name,
                     const std::string& clsname) const
{
  return Accessor<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c ConstAccessor class.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::const_reference_type
AuxElement::auxdataConst (const std::string& name) const
{
  return Accessor<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux data variable, as a const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c ConstAccessor class.
 */
template <class T, class ALLOC>
typename Accessor<T, ALLOC>::const_reference_type
AuxElement::auxdataConst (const std::string& name,
                          const std::string& clsname) const
{
  return Accessor<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Check if an aux variable is available for reading
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
bool AuxElement::isAvailable (const std::string& name,
                              const std::string& clsname /*= ""*/) const
{
  return Accessor<T, ALLOC>(name, clsname).isAvailable(*this);
}


/**
 * @brief Check if an aux variable is available for writing
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
bool AuxElement::isAvailableWritable (const std::string& name,
                                      const std::string& clsname /*= ""*/)
{
  return Accessor<T, ALLOC>(name, clsname).isAvailableWritable(*this);
}


/**
 * @brief Check if an aux variable is available for writing as a decoration.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 */
template <class T, class ALLOC>
bool AuxElement::isAvailableWritableAsDecoration (const std::string& name,
                                                  const std::string& clsname /*= ""*/) const
{
  return Decorator<T, ALLOC>(name, clsname).isAvailableWritable(*this);
}


/**
 * @brief Fetch an aux decoration, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
typename Decorator<T, ALLOC>::reference_type
AuxElement::auxdecor (const std::string& name) const
{
  return Decorator<T, ALLOC>(name, "")(*this);
}


/**
 * @brief Fetch an aux decoration, as a non-const reference.
 * @param name Name of the aux variable.
 * @param clsname The name of the associated class.  May be blank.
 *
 * This method has to translate from the aux data name to the internal
 * representation each time it is called.  Using this method
 * inside of loops is discouraged; instead use the @c Accessor class.
 *
 * If the container is locked, this will allow fetching only variables
 * that do not yet exist (in which case they will be marked as decorations)
 * or variables already marked as decorations.
 */
template <class T, class ALLOC>
typename Decorator<T, ALLOC>::reference_type
AuxElement::auxdecor (const std::string& name,
                      const std::string& clsname) const
{
  return Decorator<T, ALLOC>(name, clsname)(*this);
}


/**
 * @brief Create a new private store for this object and copy aux data.
 * @param other The object from which aux data should be copied.
 *
 * @c ExcBadPrivateStore will be thrown if this object is already
 * associated with a store.
 *
 * If @c other is an object that has aux data, then those data will
 * be copied; otherwise, nothing will be done.
 */
template <class U1>
inline
void AuxElement::makePrivateStore (const U1& other)
{
  // Dispatch to the proper overload depending on whether or not
  // other derives from AuxElement.
  makePrivateStore1 (&other);
}


/**
 * @brief Create a new private store for this object and copy aux data.
 * @param other The object from which aux data should be copied.
 *
 * @c ExcBadPrivateStore will be thrown if this object is already
 * associated with a store.
 *
 * If @c other is an object that has aux data, then those data will
 * be copied; otherwise, nothing will be done.
 */
template <class U1>
inline
void AuxElement::makePrivateStore (const U1* other)
{
  // Dispatch to the proper overload depending on whether or not
  // other derives from AuxElement.
  makePrivateStore1 (other);
}


/**
 * @brief Synonym for @c setStore with @c IConstAuxStore.
 * @param store The new store.
 */
inline
void AuxElement::setConstStore (const SG::IConstAuxStore* store)
{
  setStore (store);
}


/**
 * @brief Synonym for @c setStore with @c IAuxStore.
 * @param store The new store.
 */
inline
void AuxElement::setNonConstStore (SG::IAuxStore* store)
{
  setStore (store);
}


/**
 * @brief Return true if index tracking is enabled for this object.
 *
 * Always returns true.  Included here to be consistent with AuxVectorBase
 * when standalone objects may be used as template parameters.
 */
inline
bool AuxElement::trackIndices() const
{
  return true;
}


/**
 * @brief Set the index/container for this element.
 * @param index The index of this object within the container.
 * @param container The container holding this object.
 *                  May be null if this object is being removed
 *                  from a container.
 *
 * Usually this simply sets the index and container members
 * of this object.  However, in the case where this object has
 * an associated private store, then we need to deal with
 * releasing the store if the object is being added to a container,
 * or making a new store if the object is being removed from a container.
 */
inline
void AuxElement::setIndex (size_t index, SG::AuxVectorData* container)
{
#ifdef ATHCONTAINERS_R21_COMPAT
  if (ATHCONTAINERS_UNLIKELY (!noPrivateData())) {
    // out-of-line piece, dealing with private store.
    setIndexPrivate (index, container);
    return;
  }

  IAuxElement::setIndex (index);
  m_container = container;
#else
  ConstAuxElement::setIndex (index, container);
#endif
}


/**
 * @brief Create a new private store for this object and copy aux data.
 * @param other The object from which aux data should be copied.
 *
 * @c ExcBadPrivateStore will be thrown if this object is already
 * associated with a store.
 *
 * This overload handles the case where @c other does not have aux data.
 */
inline
void AuxElement::makePrivateStore1 (const void*)
{
  makePrivateStore();
}


} // namespace SG
