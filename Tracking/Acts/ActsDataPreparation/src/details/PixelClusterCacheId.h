/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ACTSTRK_PIXCLUSTERCACHEID_H
#define ACTSTRK_PIXCLUSTERCACHEID_H

#include "xAODInDetMeasurement/PixelCluster.h"
#include "src/Cache.h"

CLASS_DEF(ActsTrk::Cache::Handles<xAOD::PixelCluster>::IDCBackend, 70424203, 1);
#endif