#include "JetCalibTools/JetCalibrationTool.h"
#include "JetCalibTools/JetCalibTool.h"
#include "JetCalibTools/PileupAreaCalibStep.h"
#include "JetCalibTools/JESCalibStep.h"
#include "JetCalibTools/SmearingCalibStep.h"
#include "JetCalibTools/GSCCalibStep.h"

DECLARE_COMPONENT( JetCalibrationTool )
DECLARE_COMPONENT( JetCalibTool )
DECLARE_COMPONENT( PileupAreaCalibStep )
DECLARE_COMPONENT( EtaMassJESCalibStep )
DECLARE_COMPONENT( SmearingCalibStep )
DECLARE_COMPONENT( GSCCalibStep )
